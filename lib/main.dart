import 'package:flutter/material.dart';

void main() {
  runApp(Weather());
}

class Weather extends StatefulWidget {
  @override
  State<Weather> createState() => _Weather();
}

class _Weather extends State<Weather> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: buildAppBarWidget(),
          body: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(32),
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      "https://i.redd.it/ihfnlpbze7o01.jpg"
                  ),
                  fit: BoxFit.cover
              ),

            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children:[
                Icon(Icons.location_on,color: Colors.white,),
                Text('อำเภอเมืองชลบุรี' ,style: const TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight:  FontWeight.w200
                ),),Text('26°', textAlign: TextAlign.center,style: const TextStyle(
                    color: Colors.white,
                    fontSize: 80,
                    fontWeight:  FontWeight.w500
                ),),
                Text('30°/22° อุณหภูมิที่รุ้สึก 28°', style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight:  FontWeight.w800
                ),),
                const SizedBox(
                  height: 48,
                ),
                const SizedBox(
                  height: 15,
                ),
                Container(
                    margin: EdgeInsets.all(0),
                  height:110,
                  width:double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black38,
                    borderRadius: BorderRadius.circular(20), //border corner radius
                    boxShadow:[
                      BoxShadow(
                        color: Colors.white.withOpacity(0), //color of shadow
                        spreadRadius: 5, //spread radius
                        blurRadius: 7, // blur radius
                        offset: Offset(0, 2), // changes position of shadow

                      ),
                    ],
                  ),
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      weatherAlwaysOn(),
                    ],
                  )
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(0),
                  height: 80,
                  width:double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black38,
                    borderRadius: BorderRadius.circular(20), //border corner radius
                    boxShadow:[
                      BoxShadow(
                        color: Colors.white.withOpacity(0), //color of shadow
                        spreadRadius: 5, //spread radius
                        blurRadius: 7, // blur radius
                        offset: Offset(0, 2), // changes position of shadow

                      ),
                    ],
                  ),
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('อุณหภูมิของพรุ่งนี้',
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                            height:2.3,
                          ),
                        ),
                        Text('คาดว่าจะเท่ากับวันนี้',
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white30,
                            height:2.3,
                          ),
                        ),


                      ]
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),

                Container( margin: EdgeInsets.all(0),
                  height:250,
                  width:double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black38,
                    borderRadius: BorderRadius.circular(20), //border corner radius
                    boxShadow:[
                      BoxShadow(
                        color: Colors.white.withOpacity(0), //color of shadow
                        spreadRadius: 5, //spread radius
                        blurRadius: 7, // blur radius
                        offset: Offset(0, 2), // changes position of shadow

                      ),
                    ],
                  ),
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        buildWeatherYesterday(),
                        buildWeatherToday(),
                        buildWeatherTomorrow(),
                        buildWeather01(),
                        buildWeather02(),


                      ]
                  ),),
              ],
            ),
          ),

        )
    );
  }
}
Widget buildWeatherYesterday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('เมื่อวาน', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white30,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white30,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('31°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white30,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('22°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white30,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),

      ]
  );
}

Widget buildWeatherToday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[

        Text('วันนี้      ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('30°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('22°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

Widget buildWeatherTomorrow(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[

        Text('พุธ        ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('31°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('23°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

Widget buildWeather01(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[

        Text('พฤหัส   ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('30°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('23°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

Widget buildWeather02(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('ศุกร์       ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('28°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('22°', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}


AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.deepPurpleAccent,
    leading: IconButton(
        onPressed: () {},
        icon: Icon(
          Icons.menu,
          color: Colors.black,
        )
    ),
    //title: Text("weather"),

  );
}
Widget weatherAlwaysOn() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildWeather1(),
      buildWeather2(),
      buildWeather3(),
      buildWeather4(),
      buildWeather5(),
      buildWeather6(),
    ],
  );
}

Widget buildWeather1() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text('23.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      const SizedBox(
        height: 10,
      ),
      Icon(
        Icons.nights_stay,
        color: Colors.white,
        // color: Colors.indigo.shade800,
      ),
      const SizedBox(
        height: 10,
      ),
      Text('26°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildWeather2() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text('00.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      const SizedBox(
        height: 10,
      ),
      Icon(
        Icons.nights_stay,
        color: Colors.white,
        // color: Colors.indigo.shade800,
      ),
      const SizedBox(
        height: 10,
      ),
      Text('26°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildWeather3() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text('01.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      const SizedBox(
        height: 10,
      ),
      Icon(
        Icons.nights_stay,
        color: Colors.white,
        // color: Colors.indigo.shade800,
      ),
      const SizedBox(
        height: 10,
      ),
      Text('25°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildWeather4() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text('02.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      const SizedBox(
        height: 10,
      ),
      Icon(
        Icons.nights_stay,
        color: Colors.white,
        // color: Colors.indigo.shade800,
      ),
      const SizedBox(
        height: 10,
      ),
      Text('25°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildWeather5() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text('03.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      const SizedBox(
        height: 10,
      ),
      Icon(
        Icons.nights_stay,
        color: Colors.white,
        // color: Colors.indigo.shade800,
      ),
      const SizedBox(
        height: 10,
      ),
      Text('24°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildWeather6() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text('04.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      const SizedBox(
        height: 10,
      ),
      Icon(
        Icons.nights_stay,
        color: Colors.white,
        // color: Colors.indigo.shade800,
      ),
      const SizedBox(
        height: 10,
      ),
      Text('24°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.white30,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}



